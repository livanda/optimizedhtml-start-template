# OptimizedHTML - Start HTML Template

Helps to speed up work. And it accelerates the development. Below are the packages that are used.

1. [browser-sync](https://www.npmjs.com/package/browser-sync)
2. [del](https://www.npmjs.com/package/del)
3. [gulp](https://www.npmjs.com/package/gulp)
4. [gulp-autoprefixer](https://www.npmjs.com/package/gulp-autoprefixer)
5. [gulp-clean-css](https://www.npmjs.com/package/gulp-clean-css)
6. [gulp-concat](https://www.npmjs.com/package/gulp-concat)
7. [gulp-imagemin](https://www.npmjs.com/package/gulp-imagemin)
8. [gulp-notify](https://www.npmjs.com/package/gulp-notify)
9. [gulp-pug](https://www.npmjs.com/package/gulp-pug)
10. [gulp-rename](https://www.npmjs.com/package/gulp-rename)
11. [gulp-ruby-sass](https://www.npmjs.com/package/gulp-ruby-sass)
12. [gulp-uglify](https://www.npmjs.com/package/gulp-uglify)
13. [gulp-util](https://www.npmjs.com/package/gulp-util)
14. [vinyl-ftp](https://www.npmjs.com/package/vinyl-ftp)

## installation

> For work you must have installed

- nvm
- node
- ruby sass
- npm-check-updates
- bower

### Ubuntu linux

```
# install ruby-sass and node
sudo apt-get -y install ruby-sass node

# install nvm For easy operation nodeJS 
curl -o- https://raw.githubusercontent.com/creationix/nvm/v0.33.2/install.sh | bash
# 
npm install -g npm-check-updates bower
```

## Run 

Updates and launches a new project.

```
npm start
``` 


